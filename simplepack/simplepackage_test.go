package simplepack

import (
	"fmt"
	"testing"
)

func TestGreetingFor(t *testing.T) {
	result := GetHelloWorld()
	fmt.Println(result)
	if result != "Hello world" {
		t.Fatal(result)
	}
}
