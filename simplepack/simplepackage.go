package simplepack

// GetHelloWorld returns "Hello world"
func GetHelloWorld() string {
	return "Hello world"
}

// GetNewHelloWorld returns "Hello world"
func GetNewHelloWorld() string {
	return GetHelloWorld()
}
